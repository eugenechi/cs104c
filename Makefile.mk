.DEFAULT_GOAL := all

all:

config:
	git config -l

init:
	git init
	git remote add origin git@gitlab.com:gpdowning/cs104c-fall-2021.git
	git add README.md
	git commit -m 'first commit'
	git push -u origin master

pull:
	@echo
	git pull
	git status

push:
	@echo
	git add .gitignore
	git add 00-introduction
	git add 01-graphs
	git add 02-shortest-path-i
	git add 03-shortest-path-ii
	git add 04-binary-search
	git add 05-greedy-algorithms
	git add 06-dynamic-programming-i
	git add 07-dynamic-programming-ii
	git add 08-number-theory
	git add 09-segment-trees
	git add 10-max-flow
	git add 11-union-find
	git add 12-geometry
	git add Makefile
	git add Makefile.mk
	git add README.md
	git commit -m "another commit"
	git push
	git status

status:
	@echo
	git branch
	git remote -v
	git status
